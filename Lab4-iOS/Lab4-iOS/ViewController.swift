//
//  ViewController.swift
//  Lab4-iOS
//
//  Created by Konstantin Ay on 2017-11-18.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    var audioPlayer: AVAudioPlayer = AVAudioPlayer()
    var people: [Person] = []
    var currentPerson: Person = Person(name: "", gender: "", height: 0, weight: 0, birthYear: "")
    var currentImage: String = ""

    @IBOutlet weak var starWarsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = UIColor.black
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        let cellSoundFile = Bundle.main.path(forResource: "cell-sound", ofType: "wav")
        
        do{
            try audioPlayer = AVAudioPlayer(contentsOf: URL(fileURLWithPath: cellSoundFile!))
        }catch{
            print(error)
        }
        fetchData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func fetchData(){
        var request = URLRequest(url: URL(string: "https://swapi.co/api/people/")!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            //print(response!)
            do {
                let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, Any>
                self.people.removeAll()
                self.initialiseList(json)
            } catch {
                print("error")
            }
        })
        
        task.resume()
    }
    
    func initialiseList(_ json: Dictionary<String, Any>){
        let result = json["results"]! as! NSArray
        
        for per in result {
            
            let p = per as! Dictionary<String, Any>
            let name = String(p["name"] as! String)
            let gender = String(p["gender"] as! String)
            let height = Int(p["height"] as! String)
            let mass = Int(p["mass"] as! String)
            let birthYear = String(p["birth_year"] as! String)
            
            people.append(Person(name: name, gender: gender, height: height!, weight: mass!, birthYear: birthYear))
        }
        DispatchQueue.main.async {
            self.starWarsTableView.reloadData()
            self.animateTable()
        }
    }
    
    func animateTable(){
        starWarsTableView.reloadData()
        let cells = starWarsTableView.visibleCells
        
        for i in cells{
            let cell: UITableViewCell = i as UITableViewCell
            cell.layer.transform = CATransform3DMakeScale(0.01, 0.01, 1.0)
        }
        
        var index = 0.2
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            
            UIView.animate(withDuration: index, animations: {
                cell.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
            })
            index += 0.2
        }
    }


}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.people.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        audioPlayer.play()
        
        currentPerson = Person(name: people[indexPath.row].name, gender: people[indexPath.row].gender, height: people[indexPath.row].height, weight: people[indexPath.row].weight, birthYear: people[indexPath.row].birthYear)
        
        currentImage = people[indexPath.row].name
        
        performSegue(withIdentifier: "showDetails", sender: self)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! StarWarsTableViewCell
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.cellLabel?.text = people[indexPath.row].name
        cell.cellImageView?.image = UIImage(named: people[indexPath.row].name)
    
        
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails" {
            let destination = segue.destination as! DetailViewController
            print("HERE", currentPerson.name)
            destination.sentName = currentPerson.name
            destination.sentGender = currentPerson.gender
            destination.sentYear = currentPerson.birthYear
            destination.sentHeight = currentPerson.height
            destination.sentWeight = currentPerson.weight
            destination.sentImageName = currentImage
        }
    }
}
