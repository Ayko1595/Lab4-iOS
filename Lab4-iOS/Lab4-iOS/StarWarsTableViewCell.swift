//
//  StarWarsTableViewCell.swift
//  Lab4-iOS
//
//  Created by Konstantin Ay on 2017-11-18.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit

class StarWarsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var innerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        innerView.layer.borderWidth = 0.4
        innerView.layer.borderColor = UIColor.gray.cgColor
        
        innerView.layer.shadowColor = UIColor.gray.cgColor;
        innerView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        innerView.layer.shadowRadius = 5.0;
        innerView.layer.shadowOpacity = 0.9;
        innerView.layer.masksToBounds = false;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
