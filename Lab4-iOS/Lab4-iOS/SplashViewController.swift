//
//  SplashViewController.swift
//  Lab4-iOS
//
//  Created by Konstantin Ay on 2017-11-19.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    @IBOutlet weak var logoImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        logoImageView.transform = CGAffineTransform(scaleX: 0, y: 0)
        UIView.animate(withDuration: 0.5, animations: {
            self.logoImageView.transform = CGAffineTransform.identity
            
        }, completion: { finished in
            self.performSegue(withIdentifier: "splashSegue", sender: self)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
