//
//  People.swift
//  Lab4-iOS
//
//  Created by Konstantin Ay on 2017-11-18.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import Foundation

class Person{
    var name: String
    var gender: String
    var height: Int
    var weight: Int
    var birthYear: String
    
    init(name: String, gender: String, height: Int, weight: Int, birthYear: String) {
        self.name = name
        self.gender = gender
        self.height = height
        self.weight = weight
        self.birthYear = birthYear
    }
}
