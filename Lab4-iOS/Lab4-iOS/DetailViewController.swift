//
//  DetailViewController.swift
//  Lab4-iOS
//
//  Created by Konstantin Ay on 2017-11-18.
//  Copyright © 2017 Konstantin Ay. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var upperVIew: UIView!
    @IBOutlet weak var lowerView: UIView!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var birthYearLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var portrait: UIImageView!
    
    var sentName: String = ""
    var sentGender: String = ""
    var sentYear: String = ""
    var sentWeight: Int = 0
    var sentHeight: Int = 0
    var sentImageName: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = UIColor.black
        statusBarView.backgroundColor = statusBarColor
        view.addSubview(statusBarView)

        // Do any additional setup after loading the view.
        heightLabel.text = String(sentHeight)
        weightLabel.text = String(sentWeight)
        genderLabel.text = sentGender
        birthYearLabel.text = sentYear
        nameLabel.text = sentName
        portrait.image = UIImage(named: sentImageName)
        
        setUpView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func setUpView(){
        
        upperVIew.layer.borderWidth = 0.4
        upperVIew.layer.borderColor = UIColor.gray.cgColor
        
        upperVIew.layer.shadowColor = UIColor.gray.cgColor;
        upperVIew.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        upperVIew.layer.shadowRadius = 5.0;
        upperVIew.layer.shadowOpacity = 0.9;
        upperVIew.layer.masksToBounds = false;
        
        lowerView.layer.borderWidth = 0.4
        lowerView.layer.borderColor = UIColor.gray.cgColor
        
        lowerView.layer.shadowColor = UIColor.gray.cgColor;
        lowerView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        lowerView.layer.shadowRadius = 5.0;
        lowerView.layer.shadowOpacity = 0.9;
        lowerView.layer.masksToBounds = false;
        
        let duration: Double = 1.0 //seconds
        DispatchQueue.global().async {
            for i in 0 ..< (self.sentHeight + 1) {
                let sleepTime = UInt32(duration/Double(self.sentHeight) * 1000000)
                usleep(sleepTime)
                DispatchQueue.main.async {
                    print(i)
                    self.heightLabel.text = "\(i)"
                }
            }
        }
    }
}
